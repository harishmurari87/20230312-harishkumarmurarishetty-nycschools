# 20230312-HarishKumarMurarishetty-NYCSchools



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/harishmurari87/20230312-harishkumarmurarishetty-nycschools.git
git branch -M main
git push -uf origin main
```

## Name
MVVM-Retrofit-Depedency Injection

## Description
This project loads NYCSchools into a recyclerView and it's details:
- It follows MVVM design pattern
- Added depedency injection - Dagger and Hilt
- Used retrofit for service calls
- ViewBinding is enabled and has materialdesign
