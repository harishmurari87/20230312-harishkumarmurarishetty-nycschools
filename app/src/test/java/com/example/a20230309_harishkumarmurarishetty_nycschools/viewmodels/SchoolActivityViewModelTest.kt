package com.example.a20230309_harishkumarmurarishetty_nycschools.viewmodels

import com.example.a20230309_harishkumarmurarishetty_nycschools.api.SchoolApiInterface
import com.example.a20230309_harishkumarmurarishetty_nycschools.repository.SchoolDataSource
import com.example.a20230309_harishkumarmurarishetty_nycschools.repository.SchoolRepository
import junit.framework.TestCase
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock

@ExperimentalCoroutinesApi
@RunWith(JUnit4::class)
class SchoolActivityViewModelTest : TestCase() {

    private lateinit var viewModel: SchoolActivityViewModel

    @Mock
    private lateinit var schoolApiInterface: SchoolApiInterface

    @Mock
    private lateinit var schoolRepository: SchoolRepository

    @Before
    public override fun setUp() {
        super.setUp()
        val dataSource = SchoolDataSource(schoolApiInterface)
        val repo = SchoolRepository(dataSource)
        viewModel = SchoolActivityViewModel(repo)
    }

    @Test
    fun getSchoolList() {

        viewModel.schoolList

    }
}