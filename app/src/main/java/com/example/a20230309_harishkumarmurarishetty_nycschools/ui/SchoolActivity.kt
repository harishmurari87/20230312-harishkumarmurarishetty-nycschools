package com.example.a20230309_harishkumarmurarishetty_nycschools.ui

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.example.a20230309_harishkumarmurarishetty_nycschools.adapters.ItemClick
import com.example.a20230309_harishkumarmurarishetty_nycschools.adapters.SchoolRecyclerAdapter
import com.example.a20230309_harishkumarmurarishetty_nycschools.databinding.SchoolActivityBinding
import com.example.a20230309_harishkumarmurarishetty_nycschools.repository.Result
import com.example.a20230309_harishkumarmurarishetty_nycschools.viewmodels.SchoolActivityViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SchoolActivity : AppCompatActivity() {

    private val viewModel: SchoolActivityViewModel by viewModels()

    private var _binding: SchoolActivityBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = SchoolActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)
        viewModel.getSchoolList()
        initAdapter()
    }

    private fun initAdapter() {
        val schoolAdapter = SchoolRecyclerAdapter(
            itemClick = ItemClick {
                val intent = Intent(this@SchoolActivity, DetailsActivity::class.java)
                intent.putExtra(KEY_SCHOOL, it.dbn)
                startActivity(intent)
            }
        )

        binding.recyclerView.apply {
            adapter = schoolAdapter
        }

        viewModel.schoolList.observe(this) { result ->
            when (result.status) {
                Result.Status.SUCCESS -> {
                    binding.schoolLoadingBar.isVisible = false
                    schoolAdapter.updateSchoolList(result.data)
                }

                Result.Status.ERROR -> {
                    binding.schoolLoadingBar.isVisible = false
                    binding.errorTxt.isVisible = true
                    binding.recyclerView.isVisible = false
                }

                Result.Status.LOADING -> {
                    //do nothing
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    companion object {
        const val KEY_SCHOOL = "school_dbn"
    }
}