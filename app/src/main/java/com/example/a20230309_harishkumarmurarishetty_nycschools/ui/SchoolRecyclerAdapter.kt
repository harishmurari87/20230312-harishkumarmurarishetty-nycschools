package com.example.a20230309_harishkumarmurarishetty_nycschools.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.a20230309_harishkumarmurarishetty_nycschools.R
import com.example.a20230309_harishkumarmurarishetty_nycschools.data.SchoolListResponse
import com.example.a20230309_harishkumarmurarishetty_nycschools.databinding.LayoutItemBinding

class SchoolRecyclerAdapter(
    private var itemClick: ItemClick? = null
) : ListAdapter<SchoolListResponse, SchoolRecyclerAdapter.ViewHolder>(SchoolItemDiffCallBack()) {

    private var schoolList = mutableListOf<SchoolListResponse>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.itemView.setOnClickListener {
            itemClick?.onClick(item)
        }
        holder.bind(
            item
        )
    }

    fun updateSchoolList(list: List<SchoolListResponse>?) {
        schoolList.clear()
        super.submitList(list?.let { ArrayList(it) })
        notifyDataSetChanged()
    }

    class ViewHolder private constructor(private val binding: LayoutItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(
            item: SchoolListResponse
        ) {
            val context = binding.root.context
            binding.tvSchoolName.text = item.schoolName
            binding.tvSchoolNumber.text = context.getString(R.string.text_phone_number, item.phoneNumber)

        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = LayoutItemBinding.inflate(
                    layoutInflater, parent, false
                )
                return ViewHolder(binding)
            }
        }
    }
}

class SchoolItemDiffCallBack : DiffUtil.ItemCallback<SchoolListResponse>() {
    override fun areItemsTheSame(
        oldItem: SchoolListResponse, newItem: SchoolListResponse
    ): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(
        oldItem: SchoolListResponse, newItem: SchoolListResponse
    ): Boolean {
        return oldItem == newItem
    }

}

class ItemClick(val clickListener: (item: SchoolListResponse) -> Unit) {
    fun onClick(item: SchoolListResponse) = clickListener(item)
}