package com.example.a20230309_harishkumarmurarishetty_nycschools.ui

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.example.a20230309_harishkumarmurarishetty_nycschools.R
import com.example.a20230309_harishkumarmurarishetty_nycschools.data.SchoolDataResponse
import com.example.a20230309_harishkumarmurarishetty_nycschools.databinding.DetailsActivityBinding
import com.example.a20230309_harishkumarmurarishetty_nycschools.repository.Result
import com.example.a20230309_harishkumarmurarishetty_nycschools.ui.SchoolActivity.Companion.KEY_SCHOOL
import com.example.a20230309_harishkumarmurarishetty_nycschools.viewmodels.DetailActivityViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DetailsActivity : AppCompatActivity() {

    private var _binding: DetailsActivityBinding? = null
    private val binding get() = _binding!!
    private val viewModel: DetailActivityViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = DetailsActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val dbn = intent.getStringExtra(KEY_SCHOOL)

        viewModel.getSchoolData(dbn ?: "")
        observeData()

        val actionBar = supportActionBar
        actionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun observeData() {
        viewModel.schoolData.observe(this) { result ->
            when (result.status) {
                Result.Status.SUCCESS -> {
                    if (result.data?.isNotEmpty() == true) {
                        displayData(result.data[0])
                    }
                    binding.loadingBar.isVisible = false
                }

                Result.Status.ERROR -> {
                    //show error
                    binding.dataLayout.isVisible = false
                    binding.loadingBar.isVisible = false

                    val builder =
                        AlertDialog.Builder(this).setTitle(getString(R.string.error_title))
                            .setMessage(getString(R.string.error_msg))
                            .setPositiveButton(getString(R.string.alert_btn)) { _, _ ->
                                finish()
                            }
                    builder.show()
                }

                Result.Status.LOADING -> {
                    binding.loadingBar.isVisible = true
                }
            }
        }
    }

    private fun displayData(data: SchoolDataResponse) {
        binding.dataLayout.isVisible = true
        binding.tvSchoolName.text = data.schoolName
        binding.tvSatText.text = getString(R.string.sat_test_takers, data.satTestTakers)
        binding.tvReadingScore.text = getString(R.string.read_avg_score, data.satReadingAvgScore)
        binding.tvMatchScore.text = getString(R.string.math_avg_score, data.satMathAvgScore)
        binding.tvWritingScore.text = getString(R.string.writing_avg_score, data.satWritingAvgScore)
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}