package com.example.a20230309_harishkumarmurarishetty_nycschools.repository

import com.example.a20230309_harishkumarmurarishetty_nycschools.data.SchoolDataResponse
import com.example.a20230309_harishkumarmurarishetty_nycschools.data.SchoolListResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

/**
 * fetches data from remote source
 */
class SchoolRepository @Inject constructor(
    private val schoolDataSource: SchoolDataSource
) {

    fun getSchoolList(): Flow<Result<List<SchoolListResponse>>> {
        return flow {
            val result = schoolDataSource.getSchoolList()
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

    fun getSchoolDetails(dbn: String): Flow<Result<List<SchoolDataResponse>>> {
        return flow {
            val result = schoolDataSource.getSchoolData(dbn = dbn)
            emit(result)
        }.flowOn(Dispatchers.IO)
    }
}