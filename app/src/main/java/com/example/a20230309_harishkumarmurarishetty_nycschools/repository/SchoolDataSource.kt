package com.example.a20230309_harishkumarmurarishetty_nycschools.repository

import com.example.a20230309_harishkumarmurarishetty_nycschools.api.SchoolApiInterface
import com.example.a20230309_harishkumarmurarishetty_nycschools.data.SchoolDataResponse
import com.example.a20230309_harishkumarmurarishetty_nycschools.data.SchoolListResponse
import retrofit2.Response
import javax.inject.Inject

/**
 * fetches data from remote over the network
 */
class SchoolDataSource @Inject constructor(private val apiInterface: SchoolApiInterface) {

    suspend fun getSchoolList(): Result<List<SchoolListResponse>> {
        return getResponse(
            request = { apiInterface.getSchoolList() },
            defaultErrorMessage = "Error Fetching School List"
        )
    }

    suspend fun getSchoolData(dbn: String): Result<List<SchoolDataResponse>> {
        return getResponse(
            request = { apiInterface.getSchoolDetails(dbn) },
            defaultErrorMessage = "Error Fetching School Details"
        )
    }

    private suspend fun <T> getResponse(
        request: suspend () -> Response<T>, defaultErrorMessage: String
    ): Result<T> {
        return try {
            println("I'm working in thread ${Thread.currentThread().name}")
            val result = request.invoke()
            if (result.isSuccessful) {
                return Result.success(result.body())
            } else {
                Result.error(defaultErrorMessage, null)
            }
        } catch (e: Throwable) {
            Result.error("Unknown Error", null)
        }
    }

}