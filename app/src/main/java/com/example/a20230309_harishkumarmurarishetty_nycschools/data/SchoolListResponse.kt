package com.example.a20230309_harishkumarmurarishetty_nycschools.data

import com.google.gson.annotations.SerializedName

data class SchoolListResponse(
    @SerializedName("school_name")
    val schoolName: String,
    val dbn: String,
    @SerializedName("phone_number")
    val phoneNumber: String
)