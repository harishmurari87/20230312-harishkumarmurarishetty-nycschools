package com.example.a20230309_harishkumarmurarishetty_nycschools.api

import com.example.a20230309_harishkumarmurarishetty_nycschools.data.SchoolDataResponse
import com.example.a20230309_harishkumarmurarishetty_nycschools.data.SchoolListResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface SchoolApiInterface {

    @GET("resource/s3k6-pzi2.json")
    suspend fun getSchoolList(): Response<List<SchoolListResponse>>


    @GET("resource/f9bf-2cp4.json")
    suspend fun getSchoolDetails(@Query("dbn") dbn: String): Response<List<SchoolDataResponse>>
}