package com.example.a20230309_harishkumarmurarishetty_nycschools.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.a20230309_harishkumarmurarishetty_nycschools.data.SchoolListResponse
import com.example.a20230309_harishkumarmurarishetty_nycschools.repository.Result
import com.example.a20230309_harishkumarmurarishetty_nycschools.repository.SchoolRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * viewModel fro SchoolActivity
 */
@HiltViewModel
class SchoolActivityViewModel @Inject constructor(private val schoolRepository: SchoolRepository) :
    ViewModel() {

    private var _schoolList = MutableLiveData<Result<List<SchoolListResponse>>>()
    var schoolList = _schoolList

    fun getSchoolList() {
        viewModelScope.launch {
            schoolRepository.getSchoolList().collect {
                _schoolList.value = it
            }
        }
    }
}