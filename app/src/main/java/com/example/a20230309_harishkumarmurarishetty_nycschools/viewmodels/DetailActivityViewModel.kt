package com.example.a20230309_harishkumarmurarishetty_nycschools.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.a20230309_harishkumarmurarishetty_nycschools.data.SchoolDataResponse
import com.example.a20230309_harishkumarmurarishetty_nycschools.repository.Result
import com.example.a20230309_harishkumarmurarishetty_nycschools.repository.SchoolRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * viewModel fro DetailsActivity
 */
@HiltViewModel
class DetailActivityViewModel @Inject constructor(private val schoolRepository: SchoolRepository) :
    ViewModel() {

    private var _schoolData = MutableLiveData<Result<List<SchoolDataResponse>>>()
    var schoolData = _schoolData

    fun getSchoolData(dbn: String) {
        viewModelScope.launch {
            schoolRepository.getSchoolDetails(dbn).collect {
                _schoolData.value = it
            }
        }
    }
}