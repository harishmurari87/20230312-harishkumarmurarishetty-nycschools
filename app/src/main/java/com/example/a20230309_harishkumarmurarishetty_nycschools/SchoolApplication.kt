package com.example.a20230309_harishkumarmurarishetty_nycschools

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * Application Class
 */
@HiltAndroidApp
class SchoolApplication : Application()