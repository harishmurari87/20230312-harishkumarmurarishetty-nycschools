package com.example.a20230309_harishkumarmurarishetty_nycschools.di

import com.example.a20230309_harishkumarmurarishetty_nycschools.repository.SchoolDataSource
import com.example.a20230309_harishkumarmurarishetty_nycschools.repository.SchoolRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module(includes = [NetworkModule::class])
@InstallIn(SingletonComponent::class)
class SchoolModule {

    @Provides
    fun providesSchoolRepository(schoolDataSource: SchoolDataSource): SchoolRepository {
        return SchoolRepository(schoolDataSource)
    }
}