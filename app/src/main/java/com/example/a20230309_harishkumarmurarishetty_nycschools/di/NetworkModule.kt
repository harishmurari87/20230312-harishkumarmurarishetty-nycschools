package com.example.a20230309_harishkumarmurarishetty_nycschools.di

import com.example.a20230309_harishkumarmurarishetty_nycschools.SchoolConfig
import com.example.a20230309_harishkumarmurarishetty_nycschools.api.SchoolApiInterface
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class NetworkModule {

    @Provides
    @Singleton
    fun provideSchoolApiService(okHttpClient: OkHttpClient): SchoolApiInterface {
        return Retrofit.Builder().baseUrl(SchoolConfig.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .build()
            .create(SchoolApiInterface::class.java)
    }

    @Provides
    @Singleton
    fun provideOkHttpClient(): OkHttpClient {
        return OkHttpClient.Builder().addInterceptor(HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }).build()
    }
}